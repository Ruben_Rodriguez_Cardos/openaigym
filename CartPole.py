import gym
import pandas as pd

n_same_sense = 3
max_angular_velocity = 1.0


def choose_action(observation, actions, observations):
    res = 0

    # The episode ends when the pole is more than 15 degrees (0.26 rad) from vertical, or the cart moves more than 2.4 units from the center.

    # Observation
    # 0 Cart Position - 4.8 4.8                              (-2.4,2.4 reglas)
    # 1 Cart Velocity - Inf Inf
    # 2 Pole Angle - 0.418 rad(-24deg)    0.418 rad(24 deg)  (-0.26,0.26 reglas)
    # 3 Pole Angular Velocity -Inf Inf

    # print("Observation:", observation)

    # Criterio principal velocidad
    if observation[2] <= 0:  # Pole inclinado a la izquierda, el carro debe acompañarlo
        res = 0
        if observation[3] <= -max_angular_velocity:  # El pole tiene demasiada velocidad a la izquierda, se debe reducir
            res = 0
        if observation[3] > max_angular_velocity:  # El pole tiene demasiada velocidad a la derecha, se debe reducir
            res = 1

    if observation[2] > 0:  # Pole inclinado a la derecha, el carro debe acompañarlo
        res = 1
        if observation[3] <= -max_angular_velocity:  # El pole tiene demasiada velocidad a la izquierda, se debe reducir
            res = 0
        if observation[3] > max_angular_velocity:  # El pole tiene demasiada velocidad a la derecha, se debe reducir
            res = 1

    # print("Action: ", res)

    return res


def main():
    data = pd.DataFrame()
    rewards = []

    env = gym.make('CartPole-v1')
    env._max_episode_steps = 2000

    n_of_episodes = 200

    for i_episode in range(0, n_of_episodes):
        observation = env.reset()

        total_t_episode = 0
        actions = []
        observations = []

        done = False
        t = 0

        while not done:

            #env.render()

            action = choose_action(observation, actions, observations)
            actions.append(action)

            observation, reward, done, info = env.step(action=action)  # action = 0 izquierda, action = 1 derecha
            observations.append(observation)

            total_t_episode += reward

            if done:
                print("Episode: ", i_episode)
                print("Episode finished after {} timesteps".format(t + 1))
                print("Reward: ", total_t_episode)
                rewards.append(total_t_episode)

                print("Last Observation: ", observation, "\n")
                break
            t = t + 1
    env.close()

    print("--- Results ---")
    print("Nº of episodes: ", n_of_episodes)
    print("Rewards mean: ", sum(rewards) / len(rewards))

    for reward in rewards:
        if reward < 190:
            print("Ooops, not passed.")
            break


if __name__ == "__main__":
    main()

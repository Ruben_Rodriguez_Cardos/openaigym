import gym
import matplotlib.pyplot as plt

"""
    Description:
        The agent (a car) is started at the bottom of a valley. For any given
        state the agent may choose to accelerate to the left, right or cease
        any acceleration.
    Source:
        The environment appeared first in Andrew Moore's PhD Thesis (1990).
    Observation:
        Type: Box(2)
        Num    Observation               Min            Max
        0      Car Position              -1.2           0.6
        1      Car Velocity              -0.07          0.07
    Actions:
        Type: Discrete(3)
        Num    Action
        0      Accelerate to the Left
        1      Don't accelerate
        2      Accelerate to the Right
        Note: This does not affect the amount of velocity affected by the
        gravitational pull acting on the car.
    Reward:
         Reward of 0 is awarded if the agent reached the flag (position = 0.5)
         on top of the mountain.
         Reward of -1 is awarded if the position of the agent is less than 0.5.
    Starting State:
         The position of the car is assigned a uniform random value in
         [-0.6 , -0.4].
         The starting velocity of the car is always assigned to 0.
    Episode Termination:
         The car position is more than 0.5
         Episode length is greater than 200
    """

class Car:

    def __init__(self):
        self.sense = 0 #0 Izquierda, 1 Derecha

    def change_sense(self):
        if self.sense == 0:
            self.sense = 1
        elif self.sense == 1:
            self.sense = 0

def choose_action(observation, actions, observations, car):
    res = 0

    if len(observations) > 1:
       if car.sense == 0: #Hacia la izquierda
           if observation[0] > observations[-2][0]:
               car.change_sense()


       if car.sense == 1: #Hacia la derecha
           if observation[0] < observations[-2][0]:
               car.change_sense()

    if car.sense == 0:
        res = 0
    else:
        res = 2

    if observation[1] > 0.10:
        res = 2

    return res


def main():
    rewards = []

    env = gym.make('MountainCar-v0')
    n_of_episodes = 100

    for i_episode in range(0, n_of_episodes):
        observation = env.reset()

        total_t_episode = 0
        actions = []
        observations = []

        done = False
        t = 0

        car = Car()

        while not done:

            #env.render()

            action = choose_action(observation, actions, observations, car)
            actions.append(action)

            observation, reward, done, info = env.step(action=action)  # action = 0 izquierda, action = 1 derecha
            observations.append(observation)

            total_t_episode += reward

            if done:
                print("Episode: ", i_episode)
                print("Episode finished after {} timesteps".format(t + 1))
                print("Reward: ", reward)
                rewards.append(t)
                print("Last Observation: ", observation)

                max_position = -1
                for observation in observations:
                    if observation[0] > max_position:
                        max_position = observation[0]

                print("Best position: ", max_position, "\n")
                break
            t = t + 1
    env.close()


    print("--- Results ---")
    print("Nº of episodes: ", n_of_episodes)
    print("Rewards mean: ", sum(rewards) / len(rewards))

    red_line = [110] * len(rewards)

    plt.plot(rewards)
    plt.plot(red_line, color='red')
    plt.ylabel('T`s to resolute')
    plt.show()

if __name__ == "__main__":
    main()

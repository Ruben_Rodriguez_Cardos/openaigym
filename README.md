# OpenAI Gym #

Repositorio par ala resolución de problemas propuestos en OpenAI gym https://gym.openai.com/envs/#classic_control

### Autor ###

Rubén Rodríguez Cardos, doctorando en Tecnologicas Informaticas Avanzadas en la UCLM.

### Problemas resuletos ###

* CartPole https://gym.openai.com/envs/CartPole-v1/

### Licencia ###

* Uso libre, con reconocimiento al autor

### Licence ###

Free use, with credit to the author
